package chapter6.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import chapter6.beans.User;

@WebFilter(urlPatterns = {"/edit","/setting"})
public class LoginFilter implements Filter {
	public static String INIT_PARAMETER_NAME_ENCODING = "encoding";

	public static String DEFAULT_ENCODING = "UTF-8";

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		// サーブレットを実行
		HttpSession session = ((HttpServletRequest)request).getSession();
		User loginUser = (User) ((HttpServletRequest)request).getSession().getAttribute("loginUser");
		List<String> errorMessages = new ArrayList<String>();

		if(loginUser == null) {
			errorMessages.add("ログインしてください");
			session.setAttribute("errorMessages", errorMessages);
			((HttpServletResponse)response).sendRedirect("./login");
			return;

		}
		chain.doFilter(request, response);

	}

	@Override
	public void init(FilterConfig config) {
	}

	@Override
	public void destroy() {
	}
}